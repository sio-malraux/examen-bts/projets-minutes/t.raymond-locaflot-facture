package locaflot.thomas.facture.classes;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe Contrat.
 * @author ThomasRmd
 */
public class Contrat {

    private int id;
    private String date;
    private LocalTime heure_debut;
    private LocalTime heure_fin;
    private int nb_emarcations;
    private ArrayList<Embarcation> embarcations;
    private double montant_total;

    /**
     * Gets id.
     *
     * @return the id - Integer
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id - Integer
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets date.
     *
     * @return the date - String
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets date.
     *
     * @param date - String
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Gets the start time of the contract.
     *
     * @return the start time - LocalTime
     */
    public LocalTime getHeure_debut() {
        return heure_debut;
    }

    /**
     * Sets the start time of the contract.
     *
     * @param heure_debut - LocalTime
     */
    public void setHeure_debut(LocalTime heure_debut) {
        this.heure_debut = heure_debut;
    }

    /**
     * Gets the end time of the contract.
     *
     * @return the end time - LocalTime
     */
    public LocalTime getHeure_fin() {
        return heure_fin;
    }

    /**
     * Sets the end time of the contract.
     *
     * @param heure_fin - LocalTime
     */
    public void setHeure_fin(LocalTime heure_fin) {
        this.heure_fin = heure_fin;
    }

    /**
     * Gets the number of boats.
     *
     * @return the number - Integer
     */
    public int getNb_emarcations() {
        return nb_emarcations;
    }

    /**
     * Sets the number of boats.
     *
     * @param nb_emarcations - Integer
     */
    public void setNb_emarcations(int nb_emarcations) {
        this.nb_emarcations = nb_emarcations;
    }

    /**
     * Gets List of objects embarcations of the contract.
     *
     * @return the List of objects embarcation - List<Embarcation>
     */
    public List<Embarcation> getEmbarcations() {
        return embarcations;
    }

    /**
     * Sets List of object embarcations.
     *
     * @param embarcations - List<Embarcation>
     */
    public void setEmbarcations(ArrayList<Embarcation> embarcations) {
        this.embarcations = embarcations;
    }

    /**
     * Gets full price of contract.
     *
     * @return full price - Double
     */
    public double getMontant_total() {
        return montant_total;
    }

    /**
     * Sets full price of contract.
     *
     * @param montant_total - Double
     */
    public void setMontant_total(double montant_total) {
        this.montant_total = montant_total;
    }

    /**
     * Instantiates a new Contrat by default.
     */
    public Contrat() {
        this.id = 0;
        this.date = "00-00-0000";
        this.heure_debut = LocalTime.MIN;
        this.heure_fin = LocalTime.MIN;
        this.nb_emarcations = 0;
        this.embarcations = new ArrayList<>();
        this.montant_total = 0.0;
    }

    /**
     * Instantiates a new Contrat.
     *
     * @param id             the id
     * @param date           the date
     * @param heure_debut    the start time of the contract
     * @param heure_fin      the end time of the contract
     * @param nb_emarcations the number of embarcations
     * @param embarcations   the List of object embarcations
     * @param montant_total  the full price of contract
     */
    public Contrat(int id, String date, LocalTime heure_debut, LocalTime heure_fin, int nb_emarcations, ArrayList<Embarcation> embarcations, double montant_total) {
        this.id = id;
        this.date = date;
        this.heure_debut = heure_debut;
        this.heure_fin = heure_fin;
        this.nb_emarcations = nb_emarcations;
        this.embarcations = embarcations;
        this.montant_total = montant_total;
    }
}
