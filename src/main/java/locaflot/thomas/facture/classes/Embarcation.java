package locaflot.thomas.facture.classes;


/**
 * Classe Embarcation.
 * @author ThomasRmd
 */
public class Embarcation {

    private String id;
    private String nom;
    private String type;
    private Double prix_demi_heure;
    private Double prix_heure;
    private Double prix_demi_jour;
    private Double prix_jour;
    private Double montant;

    /**
     * Gets id.
     *
     * @return the id - String
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id - String
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets nom.
     *
     * @return the name - String
     */
    public String getNom() {
        return nom;
    }

    /**
     * Sets nom.
     *
     * @param nom - String
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Gets type.
     *
     * @return the type - String
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type - String
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the price per half hour.
     *
     * @return the prix - Double
     */
    public Double getPrix_demi_heure() {
        return prix_demi_heure;
    }

    /**
     * Sets price per half hour.
     *
     * @param prix_demi_heure - Double
     */
    public void setPrix_demi_heure(Double prix_demi_heure) {
        this.prix_demi_heure = prix_demi_heure;
    }

    /**
     * Gets price per hour.
     *
     * @return the price - Double
     */
    public Double getPrix_heure() {
        return prix_heure;
    }

    /**
     * Sets price per hour.
     *
     * @param prix_heure - Double
     */
    public void setPrix_heure(Double prix_heure) {
        this.prix_heure = prix_heure;
    }

    /**
     * Gets price per half day.
     *
     * @return the price - Double
     */
    public Double getPrix_demi_jour() {
        return prix_demi_jour;
    }

    /**
     * Sets price per half day.
     *
     * @param prix_demi_jour - Double
     */
    public void setPrix_demi_jour(Double prix_demi_jour) {
        this.prix_demi_jour = prix_demi_jour;
    }

    /**
     * Gets price per day.
     *
     * @return the price - Double
     */
    public Double getPrix_jour() {
        return prix_jour;
    }

    /**
     * Sets price per day.
     *
     * @param prix_jour - Double
     */
    public void setPrix_jour(Double prix_jour) {
        this.prix_jour = prix_jour;
    }

    /**
     * Gets final price.
     *
     * @return the montant - Double
     */
    public Double getMontant() {
        return montant;
    }

    /**
     * Sets montant - calculates final price.
     */
//à voir
    public void setMontant_calculated() {
        this.montant = 0.0;
    }

    /**
     * Instantiates a new Embarcation.
     */
    public Embarcation() {
        this.id = "";
        this.nom = "";
        this.type = "";
        this.prix_demi_heure = 0.0;
        this.prix_heure = 0.0;
        this.prix_demi_jour = 0.0;
        this.prix_jour = 0.0;
        this.montant = 0.0;
    }

    /**
     * Instantiates a new Embarcation.
     *
     * @param id              the id
     * @param nom             the name
     * @param type            the type
     * @param prix_demi_heure the price per half hour
     * @param prix_heure      the price per hour
     * @param prix_demi_jour  the price per half day
     * @param prix_jour       the price per day
     */
    public Embarcation(String id, String nom, String type, Double prix_demi_heure, Double prix_heure, Double prix_demi_jour, Double prix_jour) {
        this.id = id;
        this.nom = nom;
        this.type = type;
        this.prix_demi_heure = prix_demi_heure;
        this.prix_heure = prix_heure;
        this.prix_demi_jour = prix_demi_jour;
        this.prix_jour = prix_jour;
        this.montant = 0.0;
    }
}
