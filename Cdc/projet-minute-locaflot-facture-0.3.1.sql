SET client_encoding = 'UTF8';
DROP SCHEMA "locaflot_facture" CASCADE;

CREATE SCHEMA "locaflot_facture";
CREATE TABLE "locaflot_facture"."contrat_location" (
    "id" integer NOT NULL,
    "date" "date",
    "heure_debut" time without time zone,
    "heure_fin" time without time zone
);
CREATE TABLE "locaflot_facture"."embarcation" (
    "id" character varying(4) NOT NULL,
    "couleur" character varying(15),
    "disponible" boolean,
    "type" character varying(2)
);
CREATE TABLE "locaflot_facture"."louer" (
    "id_contrat" integer NOT NULL,
    "id_embarcation" character varying(4) NOT NULL,
    "nb_personnes" integer
);
CREATE TABLE "locaflot_facture"."type_embarcation" (
    "code" character varying(2) NOT NULL,
    "nom" character varying(20),
    "nb_place" integer,
    "prix_demi_heure" numeric(5,2),
    "prix_heure" numeric(5,2),
    "prix_demi_jour" numeric(5,2),
    "prix_jour" numeric(5,2)
);
INSERT INTO "locaflot_facture"."contrat_location" VALUES (1, '2018-08-29', '10:00:00', '10:30:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (2, '2018-08-29', '10:05:00', '10:35:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (3, '2018-08-29', '10:02:00', '11:02:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (4, '2018-08-30', '10:30:00', '18:00:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (5, '2018-08-30', '10:45:00', '11:15:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (6, '2018-08-31', '11:30:00', '12:30:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (7, '2018-08-31', '11:45:00', '12:15:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (8, '2018-09-01', '13:30:00', '14:00:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (9, '2018-09-01', '13:32:00', '14:02:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (10, '2018-09-01', '13:40:00', '14:40:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (11, '2018-09-01', '13:45:00', '14:00:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (12, '2018-09-01', '13:47:00', '18:00:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (123, '2018-10-16', '12:00:00', '13:00:00');
INSERT INTO "locaflot_facture"."contrat_location" VALUES (345, '2018-10-16', '12:30:00', '13:00:00');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('B101', 'vert', true, 'B1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('B102', 'blanc', true, 'B1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('B103', 'jaune', false, 'B1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('B201', 'blanc', false, 'B2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('J101', 'multicolore', true, 'J1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('J102', 'blanc', false, 'J1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('J103', 'blanc', true, 'J1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('J201', 'jaune', false, 'J2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('J202', 'blanc', true, 'J2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('M101', 'jaune', true, 'M1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('M102', 'blanc', false, 'M1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('M103', 'blanc', false, 'M1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P101', 'blanc', true, 'P1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P102', 'blanc', true, 'P1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P103', 'jaune', false, 'P1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P104', 'bleu', true, 'P1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P105', 'rouge', false, 'P1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P106', 'blanc', true, 'P1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P107', 'bleu', true, 'P1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P108', 'jaune', false, 'P1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P109', 'rouge', true, 'P1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P110', 'jaune', true, 'P1');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P201', 'rouge', false, 'P2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P202', 'jaune', true, 'P2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P203', 'blanc', false, 'P2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P204', 'blanc', true, 'P2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P205', 'bleu', true, 'P2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P206', 'violet', false, 'P2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P207', 'blanc', true, 'P2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P208', 'jaune', true, 'P2');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P301', 'blanc', false, 'P3');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P302', 'rouge', true, 'P3');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P303', 'rouge', false, 'P3');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P304', 'jaune', true, 'P3');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P305', 'bleu', true, 'P3');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P306', 'bleu', false, 'P3');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('P307', 'bleu', false, 'P3');
INSERT INTO "locaflot_facture"."embarcation" VALUES ('B202', 'rouge', false, 'B2');
INSERT INTO "locaflot_facture"."louer" VALUES (1, 'P101', 1);
INSERT INTO "locaflot_facture"."louer" VALUES (2, 'P106', 2);
INSERT INTO "locaflot_facture"."louer" VALUES (3, 'P102', 2);
INSERT INTO "locaflot_facture"."louer" VALUES (4, 'J202', 1);
INSERT INTO "locaflot_facture"."louer" VALUES (4, 'M101', 2);
INSERT INTO "locaflot_facture"."louer" VALUES (5, 'B101', 3);
INSERT INTO "locaflot_facture"."louer" VALUES (6, 'P207', 1);
INSERT INTO "locaflot_facture"."louer" VALUES (6, 'P208', 1);
INSERT INTO "locaflot_facture"."louer" VALUES (7, 'P302', 2);
INSERT INTO "locaflot_facture"."louer" VALUES (7, 'P304', 2);
INSERT INTO "locaflot_facture"."louer" VALUES (7, 'P305', 2);
INSERT INTO "locaflot_facture"."louer" VALUES (8, 'P106', 1);
INSERT INTO "locaflot_facture"."louer" VALUES (9, 'P102', 1);
INSERT INTO "locaflot_facture"."louer" VALUES (10, 'P110', 2);
INSERT INTO "locaflot_facture"."louer" VALUES (11, 'J201', 1);
INSERT INTO "locaflot_facture"."louer" VALUES (12, 'J103', 1);
INSERT INTO "locaflot_facture"."louer" VALUES (123, 'B202', 3);
INSERT INTO "locaflot_facture"."louer" VALUES (345, 'B202', 3);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('B1', 'barque', 2, 15.00, 25.00, 80.00, 150.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('B2', 'barque', 4, 30.00, 50.00, 140.00, 200.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('J1', 'jet-ski', 1, 80.00, 150.00, 300.00, 500.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('J2', 'jet-ski', 2, 120.00, 250.00, 400.00, 700.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('M1', 'bateau à moteur', 4, 100.00, 200.00, 400.00, 800.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('P1', 'pédalo simple', 2, 30.00, 55.00, 150.00, 250.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('P2', 'pédalo double', 4, 40.00, 75.00, 170.00, 280.00);
INSERT INTO "locaflot_facture"."type_embarcation" VALUES ('P3', 'pédalo toboggan', 6, 45.00, 80.00, 180.00, 300.00);
ALTER TABLE ONLY "locaflot_facture"."contrat_location"
    ADD CONSTRAINT "pk_contrat_location" PRIMARY KEY ("id");
ALTER TABLE ONLY "locaflot_facture"."embarcation"
    ADD CONSTRAINT "pk_embarcation" PRIMARY KEY ("id");
ALTER TABLE ONLY "locaflot_facture"."louer"
    ADD CONSTRAINT "pk_louer" PRIMARY KEY ("id_contrat", "id_embarcation");
ALTER TABLE ONLY "locaflot_facture"."louer"
    ADD CONSTRAINT "louer_contrat_fkey" FOREIGN KEY ("id_contrat") REFERENCES "locaflot_facture"."contrat_location"("id");
ALTER TABLE ONLY "locaflot_facture"."louer"
    ADD CONSTRAINT "louer_embarcation_fkey" FOREIGN KEY ("id_embarcation") REFERENCES "locaflot_facture"."embarcation"("id");
ALTER TABLE ONLY "locaflot_facture"."type_embarcation"
    ADD CONSTRAINT "pk_type_embarcation" PRIMARY KEY ("code");
ALTER TABLE ONLY "locaflot_facture"."embarcation"
    ADD CONSTRAINT "fk_embarcation" FOREIGN KEY ("type") REFERENCES "locaflot_facture"."type_embarcation"("code");
GRANT USAGE ON SCHEMA "locaflot_facture" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_facture"."contrat_location" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_facture"."embarcation" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_facture"."louer" TO "etudiants-slam";
GRANT SELECT ON TABLE "locaflot_facture"."type_embarcation" TO "etudiants-slam";
